<!doctype html>
<!--[if lt IE 7]><html class="no-js ie6 oldie" lang="en"><![endif]-->
<!--[if IE 7]><html class="no-js ie7 oldie" lang="en"><![endif]-->
<!--[if IE 8]><html class="no-js ie8 oldie" lang="en"><![endif]-->
<!--[if gt IE 8]><!--><html class="no-js" <?php language_attributes(); ?>><!--<![endif]-->
<head>
	<title><?php wp_title(''); ?></title>
    <meta http-equiv="Content-Type" content="<?php bloginfo('html_type'); ?>; charset=<?php bloginfo('charset'); ?>" />
    <meta name="viewport" content="width=device-width,initial-scale=1,maximum-scale=1"/>
	<link rel="shortcut icon" href="<?php bloginfo('stylesheet_directory'); ?>/favicon.ico" />
	<link rel="alternate" type="application/rss+xml" title="RSS 2.0" href="<?php bloginfo('rss2_url'); ?>" />
	<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />
    <link rel="apple-touch-icon" href="<?php echo get_template_directory_uri(); ?>/images/apple-favicon.ico" />
    <script type="text/javascript" src="http://use.typekit.com/uwj6aos.js"></script>
	<script type="text/javascript">try{Typekit.load();}catch(e){}</script>
    <script type="text/javascript">

	  var _gaq = _gaq || [];
	  _gaq.push(['_setAccount', 'UA-28627625-1']);
	  _gaq.push(['_trackPageview']);
	
	  (function() {
	    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
	    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
	    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
	  })();
	
	</script>
	
	<?php wp_head(); ?>

</head>
<body <?php body_class(); ?>>
	
<section id="responsive">
	
<section id="news">
	<?php if (!is_page(206)): ?>
	
	<article class="push_content">
		
		<article class="wrap">
			
			<aside class="one_half first">
				<img src="<?php the_field('news_announcements_image','option');?>" alt=""/>
			</aside>
			
			<aside class="one_half second">
				<?php the_field('news_announcements','option');?>
			</aside>
			
		</article><!--end of wrap-->
		
	</article><!--end of push_content-->
	
	<article class="news_bar">
	
		<article class="wrap">
				
				<span>Holby News and Expo announcements</span><a class="push open" href="#">Details</a>
				
		</article><!--end of wrap-->
		
	</article><!--end of news_bar-->	
	
	<?php else: ?>
	
		<article class="news_bar">
		
			<article class="wrap">
					
					<a class="feedback" href="<?php echo site_url('/leave-feedback/'); ?>">Leave Feedback</a>
					
			</article><!--end of wrap-->
			
		</article><!--end of news_bar-->	
	
	<?php endif; ?>
</section><!--end of news-->

<header>
		
	<article class="wrap">
		
		<span id="logo"><a href="<?php echo home_url(); ?>"><?php wp_title(''); ?></a></span>	
		
		<?php wp_nav_menu(array('theme_location' => 'main_menu', 'container' => 'nav', 'container_id' => 'nav', 'items_wrap' => '<ul>%3$s</ul>' ) ); ?>	
		
		<?php wp_nav_menu(array('theme_location' => 'main_menu', 'container' => 'nav', 'container_id' => 'mobile', 'items_wrap' => '<ul>%3$s</ul>' ) ); ?>		
		
		<div class="clear"></div>
			
	</article><!--end of wrap-->
	
</header>

<section id="res_nav">
	<article class="wrap">
		
	</article><!--end of wrap-->	
</section><!--end of res_nav-->
