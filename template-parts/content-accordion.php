<?php

// Check rows exists.
if( have_rows('accordion') ):

    // Loop through rows.
    while( have_rows('accordion') ) : the_row();

?>

    <h5 class="toggle"><?php the_sub_field('title'); ?></h5>

    <div class="toggle-content">
        <?php
            $content_type = get_sub_field('content_type');
            if ($content_type == 'testimonials') {
                get_template_part('/template-parts/block', 'testimonial');
            } elseif ($content_type == 'files') {
                get_template_part('/template-parts/block', 'files');
            } else {
                the_sub_field('content');
            }
        ?>
    </div>

<?php

    // End loop.
    endwhile;

// No value.
else :
    // Do something...
endif;