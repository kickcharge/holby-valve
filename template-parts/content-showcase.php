<div class="gallery">
<?php 
    $attr = array(
        'post_type' => 'rl_gallery',
        'posts_per_page' => -1,
        'post_status' => 'publish',
        'orderby' => 'menu_order',
    );
    $query = new WP_Query($attr);
    if($query->have_posts()) :
        
        // Taxonomy Filters 
?>
    <select id="gallery-filter-dd" style="display:none;">
        <option value=''>Show All</option>
        <?php while($query->have_posts()) : $query->the_post();
            echo '<option class="filter" value="img-'. get_the_ID() .'">'. get_the_title().'</option>';
        endwhile; ?>
    </select>
    
    <ul class="gal">
        <?php while($query->have_posts()) : $query->the_post() ?>
            <?php $gallery = get_post_meta(get_the_ID(), '_rl_images', true)['media']['attachments']['ids']; ?>
            <?php for( $i=0; $i < count($gallery); $i++) { ?>
                <li class="img-<?php echo get_the_ID(); ?>">
                    <a rel="lightbox-gallery-<?php echo get_the_ID(); ?>" href="<?php echo wp_get_attachment_url($gallery[$i]); ?>">
                            <?php echo wp_get_attachment_image($gallery[$i]); ?>
                    </a>				                
                </li>
            <?php } ?>
        <?php endwhile; wp_reset_postdata();?>
    </ul>
<?php endif; ?>
</div><!--end of gallery-->	