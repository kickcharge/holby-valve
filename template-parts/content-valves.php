<h2>Valves</h2>
			
			<?php while(has_sub_field('valves')): ?>
			
				<h5 class="toggle"><?php the_sub_field('type'); ?></h5>
				
				<div class="toggle-content">
					
					<ul>
					<?php while(has_sub_field('valve')): ?>
						
						<li>
							<span class="item"><?php the_sub_field('item'); ?></span>
							
							<?php

							// vars	
							$documents = get_sub_field('documents'); ?>
							
							<?php if( $documents && in_array('pdf', $documents) ) : ?>
								<span class="doc"><a href="<?php the_sub_field('pdf'); ?>" target="_blank" rel="nofollow"><img src="<?php bloginfo('template_directory'); ?>/images/pdf.jpg" alt=""/></a></span>
							<?php endif; ?>
															
							<?php if( $documents && in_array('cad', $documents) ) : ?>
								<span class="doc"><a href="<?php the_sub_field('cad'); ?>" target="_blank" rel="nofollow"><img src="<?php bloginfo('template_directory'); ?>/images/cad.jpg" alt=""/></a></span>
							<?php endif; ?>
							
							<?php if( $documents && in_array('doc', $documents) ) : ?>
								<span class="doc"><a href="<?php the_sub_field('doc'); ?>" target="_blank" rel="nofollow"><img src="<?php bloginfo('template_directory'); ?>/images/doc.jpg" alt=""/></a></span>
							<?php endif; ?>
							
							<?php if( $documents && in_array('2d', $documents) ) : ?>
								<span class="doc"><a href="<?php the_sub_field('2d'); ?>" target="_blank" rel="nofollow"><img src="<?php bloginfo('template_directory'); ?>/images/cad2D.jpg" alt=""/></a></span>
							<?php endif; ?>
							
							<?php if( $documents && in_array('3d', $documents) ) : ?>
								<span class="doc"><a href="<?php the_sub_field('3d'); ?>" target="_blank" rel="nofollow"><img src="<?php bloginfo('template_directory'); ?>/images/cad3D.jpg" alt=""/></a></span>
							<?php endif; ?>
							
							<?php if( $documents && in_array('revit', $documents) ) : ?>
								<span class="doc"><a href="<?php the_sub_field('revit'); ?>" target="_blank" rel="nofollow"><img src="<?php bloginfo('template_directory'); ?>/images/revit.jpg" alt=""/></a></span>
							<?php endif; ?>			
							
					
					<?php endwhile; ?>
					</ul>
				</div>
			
			<?php endwhile; ?>	