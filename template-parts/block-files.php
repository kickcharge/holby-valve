

        <?php if (have_rows('files')) : ?>
            <?php while (have_rows('files')) : the_row(); ?>
                <?php if (have_rows('file')) : ?>
                    <ul>
                    <?php while (have_rows('file')) : the_row(); ?>
                        
                        <?php $documents_checked_values = get_sub_field('documents'); ?>
                        <?php $document = get_sub_field('document'); ?>
                        
                        <?php if ($document) : ?>
                            <li class="<?php echo esc_html($documents_checked_values[0]); ?>">
                                <span class="item"><?php the_sub_field('item_name'); ?></span>
                                <span class="doc">
                                    <a href="<?php echo esc_url($document['url']); ?>">
                                        <img src="<?php bloginfo('template_directory'); ?>/images/<?php echo esc_html($documents_checked_values[0]); ?>.jpg" alt=""/>
                                    </a>
                                </span>
                            </li>
                        <?php endif; ?>

                    <?php endwhile; ?>
                <?php else : ?>
                    <?php // No rows found 
                    ?>
                <?php endif; ?>
                </ul>
            <?php endwhile; ?>
        <?php endif; ?>