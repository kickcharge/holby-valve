<?php

// WP_Query arguments
$args = array(
	'post_type'              => array( 'testimonials' ),
	'post_status'            => array( 'publish' ),
	'order'                  => 'DESC',
	'orderby'                => 'date',
);

// The Query
$query = new WP_Query( $args );

// The Loop
if ( $query->have_posts() ) {
	while ( $query->have_posts() ) {
		$query->the_post();
?>

<div class="testimonial">
    <h3><?php the_title(); ?></h3>
    <?php the_content(); ?>
</div>

<?php
	}
} else {
	// no posts found
}

// Restore original Post Data
wp_reset_postdata();