<footer>
	
	<section id="footer_top">
	
	    <article class="wrap">
		    
		    <aside class="one_fourth">
			    	
			    <a href="<?php echo home_url(); ?>"><img src="<?php bloginfo('template_directory'); ?>/images/footerlogo.png" alt="" /></a>
			    
		    </aside><!--end of one_fourth-->
		    
		    <aside class="one_half">
			    
			    <?php wp_nav_menu(array('theme_location' => 'footer_menu', 'container' => 'nav', 'container_id' => 'footer_nav', 'items_wrap' => '<ul>%3$s</ul>' ) ); ?>
			    
		    </aside><!--end of one_half-->
		    
		    <aside class="one_fourth last">
			    
			      <?php if(is_front_page()){ ?><a href="http://www.kickcharge.com" target="_blank">Site by KickCharge Creative</a><?php } ?>
			    
		    </aside><!--end of one_fourth-->	
		    
		    <div class="clear"></div>
		    
	    </article><!--end of wrap-->
	    
	</section><!--end of footer_top-->    
	
	<section id="footer_bot">
		
		<article class="wrap">        
        	
            <ul>
            	<li>&copy; <?php echo date("Y") ?> <?php the_field('company_name','option')?></li>
                <li><?php the_field('street','option')?></li>
                <li><?php the_field('address','option')?></li>
                <li>Phone: <?php the_field('phone','option')?></li>
                <li>Fax: <?php the_field('fax','option')?></li>
                <li>Email: <a href="mailto:<?php the_field('email','option')?>"><?php the_field('email','option')?></a></li>
                <li class="social-linkedin"><a href="https://www.linkedin.com/company/holby-valve" target="_blank"><i class="fab fa-linkedin"></i></a></li>
            </ul>
            
		</article><!--end of wrap-->
		
	</section><!--end of footer_bot-->	     
        
</footer>

</section><!--end of responsive-->

<?php wp_footer(); ?>

</body>
<html>