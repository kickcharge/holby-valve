
 // homepage slider

jQuery(document).ready(function(jQuery) {
	jQuery('#slider .wrap').unslider({
		autoplay: true
	});
});

// header news push bar 

jQuery( document ).ready(function() {
    jQuery( ".push" ).click(function() {
        jQuery( ".push_content" ).toggleClass( "active" );
    });
    
    jQuery('.open').click(function(){
	    var $this = jQuery(this);
	    $this.toggleClass('open');
	    if($this.hasClass('open')){
	        $this.text('Details');         
	    } else {
	        $this.text('Close');
	    }
	});
});   


jQuery(function(){
		jQuery('#mobile').slicknav({
        label:'MENU',
        prependTo:'#res_nav .wrap',
        closeOnClick:'true', // Close menu when a link is clicked.
        closedSymbol: '&#43;',
        openedSymbol: '&#45;'
});
})

jQuery(document).ready(function($){
	if($('.page-id-13')){
		$('#gallery-filter-dd').select2({
	        minimumResultsForSearch: -1 //disables search
	    });
		$('#gallery-filter-dd').on('change', function(){
			var filter=$(this).val();
			$(".gal li").show();
			if(filter !== ''){
				$(".gal li:not(."+ filter +"").hide();
			}
		});
	}
})