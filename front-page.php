<?php get_header();?>

<section id="slider">
	
	<article class="wrap">			
		
		<ul>
			
		<?php while(has_sub_field('slider')): ?>
		
			<li><a href="<?php the_sub_field('slider_image_link'); ?>"><img src="<?php the_sub_field('slider_image'); ?>"/></a></li>
									
		<?php endwhile; ?>
		
		</ul>		
		
		<div class="clear"></div>
		
		<h5><?php the_field('slider_bottom_text')?></h5>
				
	</article>
	
</section>

<section id="columns">
	
	<article class="wrap">
		
		<?php while(has_sub_field('columns')): ?>
		
			<aside class="one_third">
				
				<a href="<?php the_sub_field('column_image_link'); ?>"><img src="<?php the_sub_field('column_image'); ?>"/></a>
				
				<span class="text"><?php the_sub_field('column_text'); ?></span>
				
			</aside><!--end of one_third-->	
				
		<?php endwhile; ?>
		
		
		
	</article><!--end of wrap-->	
	
</section><!--end of columns-->

<?php get_footer();?>