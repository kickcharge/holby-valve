<?php

//////////////////////////////////////////////////////////////////
// Custom Functions.php file for our themes
// Add / Enable WP functions and filters for our site
// Version: 1.0
// WP Test Version: 4.0
// Creation Date: 1/1/2015
// Updated Date: 
// Author: Craig Moser
//////////////////////////////////////////////////////////////////

// Lets include our WP security patch
//include_once('inc/security.php');
// End of secuirty patch

// This is required and will need to be changed with every site.
if ( ! isset( $content_width ) ) $content_width = 980; // This variable is a global variable that is used to set the width of the content on the site.
// End of content width

// Register styles and scripts in WP

function mytheme_enqueue_scripts() {

	// Register Styles
	wp_register_style('css_reset', get_template_directory_uri() . '/css/reset.css', array(), 1.6, 'screen');
    wp_register_style('theme_style', get_template_directory_uri() . '/style.css', array(), 1.0, 'screen');	
    wp_register_style('res_style', get_template_directory_uri() . '/css/slicknav.css', array(), 1.0, 'screen');
    wp_register_style('slider_style', get_template_directory_uri() . '/css/unslider.css', array(), 1.0, 'screen');
    wp_register_style('select2-css', get_template_directory_uri() . '/css/select2.min.css', array(), 1.0, 'screen');
   
   	// Register Scripts
   	wp_register_script('modernizr', get_template_directory_uri() . '/js/libs/modernizr.js', true);
    wp_register_script('script', get_template_directory_uri() . '/js/script.js', true);
    wp_register_script('res_nav', get_template_directory_uri() . '/js/libs/jquery.slicknav.min.js', true);    
    wp_register_script('slider', get_template_directory_uri() . '/js/libs/unslider.js', true);  
    wp_register_script('select2-js', get_template_directory_uri() . '/js/libs/select2.full.min.js', true);
	wp_register_script('font_awesome', 'https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.4/js/all.min.js', '', '', true);
	
	//Enqueue Styles/Scripts
    wp_enqueue_style('css_reset');
    wp_enqueue_style('theme_style');
    wp_enqueue_style('res_style'); 
    wp_enqueue_style('slider_style'); 
    wp_enqueue_style('select2-css');
       
      
	wp_enqueue_script('jquery');	
	wp_enqueue_script('modernizr');
	wp_enqueue_script('font_awesome', array('jquery'));
	wp_enqueue_script('slider', array('jquery'));
	wp_enqueue_script('res_nav', array('jquery'));
	wp_enqueue_script('script', array('jquery'));
	wp_enqueue_script('select2-js', array('jquery'));
	
}
add_action('wp_enqueue_scripts', 'mytheme_enqueue_scripts');

// add a favicon to your
function blog_favicon()
{
    echo '<link rel="SHORTCUT ICON" type="image/x-icon" href="http://holbyvalve.com/favicon.ico" />';
}

add_action('wp_head', 'blog_favicon');

//////////////////////////////////////////////////////////////////
// Conditionally add IE specific stylesheets
//////////////////////////////////////////////////////////////////

add_action('wp_head', 'ie', 20);
function ie() { ?>
    
<?php

}

//////////////////////////////////////////////////////////////////
// Load ACF fields through Yoast 3.0+ - New JS method
//////////////////////////////////////////////////////////////////



//////////////////////////////////////////////////////////////////
// Remove query strings from CSS/JS files - SEO Purposes
//////////////////////////////////////////////////////////////////

function qsr_remove_script_version( $src ){
    $parts = explode( '?ver', $src );
        return $parts[0];
}
add_filter( 'script_loader_src', 'qsr_remove_script_version', 15, 1 );
add_filter( 'style_loader_src', 'qsr_remove_script_version', 15, 1 );

//////////////////////////////////////////////////////////////////
// WordPress title tag update: https://codex.wordpress.org/Title_Tag
//////////////////////////////////////////////////////////////////

/*
 * Let WordPress manage the document title.
 * By adding theme support, we declare that this theme does not use a
 * hard-coded <title> tag in the document head, and expect WordPress to
 * provide it for us.
 */
add_theme_support( 'title-tag' );

//////////////////////////////////////////////////////////////////
// ACF Options Page
//////////////////////////////////////////////////////////////////

if( function_exists('acf_add_options_page') ) {
	
	acf_add_options_page();
	
}


//////////////////////////////////////////////////////////////////
// Sidebar Ancestor
//////////////////////////////////////////////////////////////////

if(!function_exists('get_post_top_ancestor_id')){
/**
 * Gets the id of the topmost ancestor of the current page. Returns the current
 * page's id if there is no parent.
 *
 * @uses object $post
 * @return int
 */
function get_post_top_ancestor_id(){
    global $post;

    if($post->post_parent){
        $ancestors = array_reverse(get_post_ancestors($post->ID));
        return $ancestors[0];
    }

    return $post->ID;
}}

function is_tree($pid)
{
  global $post;

  $ancestors = get_post_ancestors($post->$pid);
  $root = count($ancestors) - 1;
  $parent = $ancestors[$root];

  if(is_page() && (is_page($pid) || $post->post_parent == $pid || in_array($pid, $ancestors)))
  {
    return true;
  }
  else
  {
    return false;
  }
};

//////////////////////////////////////////////////////////////////
// Register Menu
//////////////////////////////////////////////////////////////////

register_nav_menus(array(       
    'main_menu' => 'Main Menu',
    'footer_menu' => 'Footer Menu',
    'mobile_menu' => 'Mobile Menu',
    'side_menu' => 'Side Menu'    
));

//////////////////////////////////////////////////////////////////
// This allows the ability to use the Featured Image functionality in Wordpress to create thumbnails for posts.
//////////////////////////////////////////////////////////////////

add_theme_support('post-thumbnails');
add_filter('post_thumbnail_html', 'my_post_image_html', 10, 3);
function my_post_image_html($html, $post_id, $post_image_id)
{
    $html = '<a href="' . get_permalink($post_id) . '" title="' . esc_attr(get_post_field('post_title', $post_id)) . '">' . $html . '</a>';
    return $html;
}




//////////////////////////////////////////////////////////////////
// Register Widget Areas
//////////////////////////////////////////////////////////////////

if (function_exists('register_sidebar')) {
    register_sidebar(array(
        'name' => 'Sidebar Right',
        'id' => 'sidebar1',
        'before_widget' => '<article id="%1$s" class="widget %2$s">',
        'after_widget' => '</article>',
        'before_title' => '<h2 class="offscreen">',
        'after_title' => '</h2>',
    ));

    register_sidebar(array(
        'name' => 'Sidebar Left',
        'id' => 'sidebar2',
        'before_widget' => '<article id="%1$s" class="widget %2$s">',
        'after_widget' => '</article>',
        'before_title' => '<h2 class="offscreen">',
        'after_title' => '</h2>',
    ));

}
//////////////////////////////////////////////////////////////////
// Add Custom Shortcodes
//////////////////////////////////////////////////////////////////

include_once('inc/shortcodes/shortcodes.php');


//////////////////////////////////////////////////////////////////
// Adjust the body class
//////////////////////////////////////////////////////////////////

/*
	Version: 2.2

*/
if( !class_exists('Awebsome_Browser_Selector') ):
/**
 * Awebsome Browser Selector
 *
 * @since 2.0
 */
class Awebsome_Browser_Selector
{
	/**
	 * PHP5 Constructor
	 */
	public function __construct()
	{
		add_filter('body_class', array(&$this, 'add_body_classes'));

		// BuddyPress support
		add_action('bp_include', array(&$this, 'bp_activate'));
	}

	/**
	 * Only load code that needs BuddyPress to run once BP is loaded and initialized
	 *
	 * @since 2.0
	 */
	public function bp_activate()
	{
		require(dirname(__FILE__) .'/'. __FILE__);
	}

	/**
	 * Parses the user agent string into browser, version and platform
	 *
	 * @author Jesse G. Donat <donatj@gmail.com>
	 * @link https://github.com/donatj/PhpUserAgent
	 * @param string $ua
	 * @return array an array with browser, version and platform keys
	 *
	 * @since 2.0
	 */
	public function parse_UA_string($ua = null)
	{
		if( is_null($ua) ) $ua = $_SERVER['HTTP_USER_AGENT'];

		$data = array(
			'platform' => '',
			'browser'  => '',
			'version'  => '',
		);

		if( preg_match('/\((.*?)\)/im', $ua, $regs) )
		{
			/*
			(?P<platform>Android|CrOS|iPhone|iPad|Linux|Macintosh|Windows\ Phone\ OS|Windows|Silk|linux-gnu|BlackBerry|Xbox)
			(?:\ [^;]*)?
			(?:;|$)
			*/
			preg_match_all('/(?P<platform>Android|CrOS|iPhone|iPad|Linux|Macintosh|Windows\ Phone\ OS|Windows|Silk|linux-gnu|BlackBerry|Nintendo\ Wii|Xbox)(?:\ [^;]*)?(?:;|$)/imx', $regs[1], $result, PREG_PATTERN_ORDER);

			$priority = array('Android', 'Xbox');

			$result['platform'] = array_unique($result['platform']);

			if( count($result['platform']) > 1 )
			{
				if( $keys = array_intersect($priority, $result['platform']) ) $data['platform'] = reset($keys);
				else $data['platform'] = $result['platform'][0];
			}
			elseif( isset($result['platform'][0]) ) $data['platform'] = $result['platform'][0];
		}

		if( $data['platform'] == 'linux-gnu' ) $data['platform'] = 'Linux';
		if( $data['platform'] == 'CrOS' ) $data['platform'] = 'Chrome OS';

		/*
		(?<browser>Camino|Kindle|Kindle\ Fire\ Build|Firefox|Safari|MSIE|AppleWebKit|Chrome|IEMobile|Opera|Silk|Lynx|Version|Wget|curl|PLAYSTATION\ \d+)
		(?:;?)
		(?:(?:[/\ ])(?<version>[0-9.]+)|/(?:[A-Z]*))
		*/
		preg_match_all('%(?P<browser>Camino|Kindle|Kindle\ Fire\ Build|Firefox|Safari|MSIE|AppleWebKit|Chrome|IEMobile|Opera|Silk|Lynx|Version|Wget|curl|PLAYSTATION\ \d+)(?:;?)(?:(?:[/ ])(?P<version>[0-9.]+)|/(?:[A-Z]*))%x',
			$ua, $result, PREG_PATTERN_ORDER);

		$key = 0;

		$data['browser'] = $result['browser'][0];
		$data['version'] = $result['version'][0];

		if( ($key = array_search('Kindle Fire Build', $result['browser'])) !== false || ($key = array_search('Silk', $result['browser'])) !== false )
		{
			$data['browser']  = $result['browser'][$key] == 'Silk' ? 'Silk' : 'Kindle';
			$data['platform'] = 'Kindle Fire';

			if( !($data['version']  = $result['version'][$key]) ) $data['version'] = $result['version'][array_search( 'Version', $result['browser'] )];
		}
		elseif( ($key = array_search('Kindle', $result['browser'])) !== false )
		{
			$data['browser']  = $result['browser'][$key];
			$data['platform'] = 'Kindle';
			$data['version']  = $result['version'][$key];
		}
		elseif( $result['browser'][0] == 'AppleWebKit' )
		{
			if( ( $data['platform'] == 'Android' && !($key = 0) ) || $key = array_search('Chrome', $result['browser']) )
			{
				$data['browser'] = 'Chrome';

				if( ($vkey = array_search('Version', $result['browser'])) !== false ) $key = $vkey;
			}
			elseif( $data['platform'] == 'BlackBerry' )
			{
				$data['browser'] = 'BlackBerry Browser';

				if( ($vkey = array_search('Version', $result['browser'])) !== false ) $key = $vkey;
			}
			elseif( $key = array_search('Safari', $result['browser']) )
			{
				$data['browser'] = 'Safari';

				if( ($vkey = array_search('Version', $result['browser'])) !== false ) $key = $vkey;
			}

			$data['version'] = $result['version'][$key];
		}
		elseif( ($key = array_search('Opera', $result['browser'])) !== false )
		{
			$data['browser'] = $result['browser'][$key];
			$data['version'] = $result['version'][$key];

			if( ($key = array_search('Version', $result['browser'])) !== false ) $data['version'] = $result['version'][$key];
		}
		elseif( $result['browser'][0] == 'MSIE' )
		{
			if( $key = array_search('IEMobile', $result['browser']) ) $data['browser'] = 'IEMobile';
			else
			{
				$data['browser'] = 'MSIE';
				$key = 0;
			}

			$data['version'] = $result['version'][$key];
		}
		elseif( $key = array_search('PLAYSTATION 3', $result['browser']) !== false )
		{
			$data['platform'] = 'PLAYSTATION 3';
			$data['browser']  = 'NetFront';
		}

		return $data;
	}

	/**
	 * Converts the parsed User Agent string to CSS classes
	 *
	 * @param $data array Server User Agent parsed
	 * @return string     Parsed CSS classes (platform + browser + version)
	 *
	 * @since 2.0
	 */
	public function parse_UA_to_classes($data)
	{
		$css['platform'] = self::filter_platform($data['platform']);
		$css['browser']  = self::filter_browser($data['browser']);
		$css['version']  = self::filter_version($data['version']);

		return join(' ', $css);
	}

	/**
	 * Filters the Platform CSS string
	 *
	 * @param $platform string Server User Agent Platform parsed
	 * @return string          CSS Platform class
	 *
	 * @since 2.0
	 */
	public function filter_platform($platform)
	{
		$p = '';

		# Android|CrOS|iPhone|iPad|Linux|Macintosh|Windows\ Phone\ OS|Windows|Silk|linux-gnu|BlackBerry|Xbox
		switch($platform)
		{
			// desktop
			case 'Windows'   : $p = 'win';  break;
			case 'Linux'     : $p = 'lnx';  break;
			case 'Macintosh' : $p = 'mac';  break;
			case 'ChromeOS'  : $p = 'cros'; break;

			// mobile
			case 'Android'          : $p = 'android';    break;
			case 'iPhone'           : $p = 'iphone';     break;
			case 'iPad'             : $p = 'ipad';       break;
			case 'Windows Phone OS' : $p = 'winphone';   break;
			case 'Kindle'           : $p = 'kindle';     break;
			case 'Kindle Fire'      : $p = 'kindlefire'; break;
			case 'BlackBerry'       : $p = 'blackberry'; break;

			// consoles
			case 'Xbox'          : $p = 'xbox'; break;
			case 'PLAYSTATION 3' : $p = 'ps3';  break;
			case 'Nintendo Wii'  : $p = 'wii'; break;

			default : break;
		}

		return $p;
	}

	/**
	 * Filters the Browser CSS string
	 *
	 * @param $browser string Server User Agent Browser parsed
	 * @return string         CSS Browser class
	 *
	 * @since 2.0
	 */
	public function filter_browser($browser)
	{
		$b = '';

		# Camino|Kindle|Kindle\ Fire\ Build|Firefox|Safari|MSIE|AppleWebKit|Chrome|IEMobile|Opera|Silk|Lynx|Version|Wget|curl|PLAYSTATION\
		switch($browser)
		{
			case 'Camino'            : $b = 'camino';   break;
			case 'Kindle'            : $b = 'kindle';   break;
			case 'Firefox'           : $b = 'firefox';  break;
			case 'Safari'            : $b = 'safari';   break;
			case 'Internet Explorer' : $b = 'ie';       break;
			case 'IEMobile'          : $b = 'iemobile'; break;
			case 'Chrome'            : $b = 'chrome';   break;
			case 'Opera'             : $b = 'opera';    break;
			case 'Silk'              : $b = 'silk';     break;
			case 'Lynx'              : $b = 'lynx';     break;
			case 'Wget'              : $b = 'wget';     break;
			case 'Curl'              : $b = 'curl';     break;

			default : break;
		}

		return $b;
	}

	/**
	 * Filters the Version CSS string
	 *
	 * @param $browser string Server User Agent Version parsed
	 * @return string         CSS Version class
	 *
	 * @since 2.0
	 */
	public function filter_version($version)
	{
		$v = explode('.', $version);

		return !empty($v[0]) ? 'v'. $v[0] : '';
	}

	/**
	 * Callback function for the body_class filter
	 *
	 * @param $classes array Body tag classes
	 * @return array         Body tag classes + parsed UA classes
	 *
	 * @since 2.0
	 */
	function add_body_classes($classes)
	{
		$classes[] = self::parse_UA_to_classes( self::parse_UA_string() );

		return $classes;
	}
} // class
endif;


//////////////////////////////////////////////////////////////////
// Sidebar Ancestor
//////////////////////////////////////////////////////////////////

if(!function_exists('get_post_top_ancestor_id')){
/**
 * Gets the id of the topmost ancestor of the current page. Returns the current
 * page's id if there is no parent.
 *
 * @uses object $post
 * @return int
 */
function get_post_top_ancestor_id(){
    global $post;

    if($post->post_parent){
        $ancestors = array_reverse(get_post_ancestors($post->ID));
        return $ancestors[0];
    }

    return $post->ID;
}}

//////////////////////////////////////////////////////////////////
// Excerpt Specific Number
//////////////////////////////////////////////////////////////////

add_action( 'init', 'my_add_excerpts_to_pages' );
function my_add_excerpts_to_pages() {
     add_post_type_support( 'page', 'excerpt' );
}

function get_excerpt($count){
  $permalink = get_permalink($post->ID);
  $excerpt = get_the_content();
  $length = 50;
  $copy = get_the_content();
  $excerpt = strip_tags($excerpt);
  $excerpt = strip_shortcodes($excerpt);
  $excerpt = substr($excerpt, 0, $count);
  $excerpt = substr($excerpt, 0, strripos($excerpt, " "));
  $excerpt = $excerpt.'... <a href="'.$permalink.'">Read More</a>'; 
  if (strlen($copy) > $length) {
	return $excerpt; 
} else { 
	return ''; 	
}
}

//////////////////////////////////////////////////////////////////
// We want to custom theme this administration panel
//////////////////////////////////////////////////////////////////

function custom_loginlogo() {
		echo '
		<style type="text/css">
			h1 a { background-image: url('.get_stylesheet_directory_uri().'/images/logo.png) !important; min-width: 300px !important; min-height: 100px !important; margin: 0 auto !important; position: relative !important; z-index: 10000 !important; background-size: auto !important; }
		</style>';
	}

	add_action('login_head', 'custom_loginlogo');

?>

