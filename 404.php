<?php get_header();?>

<section id="banner">
	
	<article class="wrap">
		
		<aside class="one_half">
		
			<?php echo get_the_post_thumbnail($post_id, 'full'); ?>
				
		</aside><!--end of one_half-->
		
		<aside class="one_half last">
			
			<ul>
				
			<?php while(has_sub_field('buttons','option')): ?>
			
				<li><a href="<?php the_sub_field('button_link','option'); ?>"><img src="<?php the_sub_field('button_image','option'); ?>" alt="" /></a></li>
							
			<?php endwhile; ?>
			
			</ul>
			
		</aside><!--end of one_half-->
		
	</article><!--end of wrap-->
	
</section><!--end of banner-->

<section id="container">
	
	<article class="wrap" style="background: url(<?php the_field('background');?>) no-repeat 475px 25px;">
		
		<aside id="left"> 
			
		<h4>Sorry Nothing Found</h4>
		
			<li style="list-style: none;" id="pages">
			   <form action="<?php bloginfo('url'); ?>" method="get">
			   <?php wp_dropdown_pages(); ?>
			   <input type="submit" name="submit" value="view" />
			   </form>
			</li>  	
		
		</aside><!--end of left-->
		
	</article><!--end of wrap-->
	
</section><!--end of container-->

<?php get_footer();?>