<?php 
// change header to css
header("Content-type: text/css");

if ( !defined('WP_LOAD_PATH') ) {
	/** classic root path if wp-content and plugins is below wp-config.php */
	$classic_root = '../../../../../' ;
	
	if (file_exists( $classic_root . 'wp-load.php') )
		define( 'WP_LOAD_PATH', $classic_root);
	else
		if (file_exists( $path . 'wp-load.php') )
			define( 'WP_LOAD_PATH', $path);
		else
			exit("Could not find wp-load.php");
}

// let's load WordPress
require_once( WP_LOAD_PATH . 'wp-load.php');
?>

.line {	border-top:1px solid <?php if(get_option('of_theme_borders')) { echo get_option('of_theme_borders'); } else { echo "#e4e4e4"; } ?>; clear: both; }
.thick-line { border-top:6px solid <?php if(get_option('of_theme_borders')) { echo get_option('of_theme_borders'); } else { echo "#e4e4e4"; } ?>; clear: both; }

/* ----------------- notification boxes ----------------- */

.greenbox {	border:1px solid #cbe2ab; background:#eefcda url(images/tick_32.png) no-repeat 20px 22px; }
.greenbox, .greenbox a { color:#6a8829; }

.bluebox { border:1px solid #bad4f6; background:#e7f5ff url(images/info_32.png) no-repeat 20px 22px; }
.bluebox, .bluebox a { color:#3c64a6; }

.yellowbox { border:1px solid #f5d788; background:#fff4d0 url(images/warning_32.png) no-repeat 20px 22px; }
.yellowbox, .yellowbox a { color:#c57f11; }

.redbox { border:1px solid #fab1b1; background:#ffe8e8 url(images/delete_32.png) no-repeat 20px 22px; }
.redbox, .redbox a { color:#bb1010; }

/* ----------------- message box types ----------------- */

.box-information { border:1px solid #bad4f6; background:#e7f5ff url(images/info_32.png) no-repeat 20px 18px; }
.box-information, .box-information a { color:#3c64a6; }

.box-confirmation {	border:1px solid #cbe2ab; background:#eefcda url(images/tick_32.png) no-repeat 20px 18px; }
.box-confirmation, .box-confirmation a { color:#6a8829; }

.box-warning { border:1px solid #f5d788; background:#fff4d0 url(images/warning_32.png) no-repeat 20px 18px; }
.box-warning, .box-warning a { color:#c57f11; }

.box-error { border:1px solid #fab1b1; background:#ffe8e8 url(images/delete_32.png) no-repeat 20px 18px; }
.box-error, .box-error a { color:#bb1010; }

/* ----------------- blockquote and pullquotes styles ----------------- */

blockquote, .pullquote-left, .pullquote-right { border-left:8px solid <?php if(get_option('of_body_link')) { echo get_option('of_body_link'); } else { echo "#fa5301"; } ?>; }