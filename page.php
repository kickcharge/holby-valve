<?php
    get_header();
    $post_id = get_the_ID();
?>

<section id="banner">
	<article class="wrap">
		<aside class="one_half">
			<?php echo get_the_post_thumbnail( $post_id, 'full' ); ?>
		</aside><!--end of one_half-->
		
		<aside class="one_half last">
			<ul>
			<?php while(has_sub_field('buttons','option')): ?>
				<li>
                    <a href="<?php the_sub_field('button_link','option'); ?>"><img src="<?php the_sub_field('button_image','option'); ?>" alt="" /></a>
                </li>
			<?php endwhile; ?>
			</ul>
		</aside><!--end of one_half-->
		
	</article><!--end of wrap-->
</section><!--end of banner-->

<section id="container">
	<article class="wrap" style="background: url(<?php the_field('background');?>) no-repeat 475px 25px;">
		<aside id="left"> 
		<?php
            if (is_page(16) ) :
                get_template_part('template-parts/content', 'valves');
            endif;

            if (is_page(1270) ) :
                get_template_part('template-parts/content', 'accordion');
            endif;

            if (have_posts()) : while (have_posts()) : the_post();
                the_content();
            endwhile;

		else :
        ?>
			<h2>Sorry Nothing Found</h2>
		<?php
            endif;
            if (is_page(13) ) :
                get_template_part('template-parts/content', 'showcase');
		    endif;
        ?>
		</aside><!--end of left-->
	</article><!--end of wrap-->
</section><!--end of container-->

<?php get_footer(); ?>