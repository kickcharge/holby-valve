<?php get_header();?>

<section id="banner">
	
	<article class="wrap">
		
		<aside class="one_half">
		
			<?php echo get_the_post_thumbnail($post_id, 'full'); ?>
				
		</aside><!--end of one_half-->
		
		<aside class="one_half last">
			
			<ul>
				
			<?php while(has_sub_field('buttons','option')): ?>
			
				<li><a href="<?php the_sub_field('button_link','option'); ?>"><img src="<?php the_sub_field('button_image','option'); ?>" alt="" /></a></li>
							
			<?php endwhile; ?>
			
			</ul>
			
		</aside><!--end of one_half-->
		
	</article><!--end of wrap-->
	
</section><!--end of banner-->

<section id="container">
	
	<article class="wrap" style="background: url(<?php the_field('background');?>) no-repeat 475px 25px;">
		
		<aside id="left"> 
			
		<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>			
		
			<article class="blog blog-<?php the_ID(); ?>" <?php post_class(); ?>>			
					
					<h2><a href="<?php echo the_permalink(''); ?>"><?php the_title(); ?></a></h2>
					
					<ul>
						<li><?php the_time(get_option('date_format')); ?></li>															
						
					</ul>
							
					
					<p><?php the_excerpt(); ?></p>
																						
			</article><!--end of .blog-->
		
		<?php endwhile; endif; ?>
								
		<?php wp_reset_query(); ?>  			
								
		<?php if(function_exists('tw_pagination')) tw_pagination(); ?>					
		
		</aside><!--end of left-->
		
	</article><!--end of wrap-->
	
</section><!--end of container-->

<?php get_footer();?>